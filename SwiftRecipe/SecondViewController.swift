//
//  SecondViewController.swift
//  SwiftRecipe
//
//  Created by Canyon Duncan on 9/29/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

   
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var recipeTitle: UILabel!
    
    @IBOutlet weak var recipeText: UITextView!
    var infoDictonary: Dictionary<String, AnyObject> = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(infoDictonary)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        recipeTitle.text = infoDictonary["Title"] as? String
        recipeText.text = infoDictonary["Description"] as? String
        let url = URL(string: (infoDictonary["Image"] as? String)!)!

        let data = try? Data(contentsOf: url) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        
        
        
        //let url = URL(string: "http://i.imgur.com/w5rkSIj.jpg")
        //let data = try? Data(contentsOf: url)
        
        if data != nil{
            recipeImage.image = UIImage(data: data!)
        }
        
        
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
