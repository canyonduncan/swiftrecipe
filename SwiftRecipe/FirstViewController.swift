//
//  ViewController.swift
//  SwiftRecipe
//
//  Created by Canyon Duncan on 9/29/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    var infoDictonary: Dictionary<String, AnyObject> = ["Title":"help" as AnyObject]
    let plistScource:String = Bundle.main.path(forResource: "recipe", ofType: "plist")!
    var plistData:NSArray?
    
    let list = ["milk", "butter", "cheese"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        //let recipes = NSArray(contentsOfFile: filePath!)
        plistData = NSArray(contentsOfFile: plistScource)
    
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return plistData!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        let dictoanry = plistData![indexPath.row] as? Dictionary<String, AnyObject>
        cell.textLabel?.text = dictoanry!["Title"] as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.infoDictonary = (plistData![indexPath.row] as? Dictionary<String, AnyObject>)!
        performSegue(withIdentifier: "showRecipe", sender: nil)
        
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        //let destinationViewController = segue.destination as? SecondViewController
        let destView : SecondViewController = (segue.destination as? SecondViewController)!
        //navigationController?.pushViewController(destination, animated: true)
        //destinationViewController?.recipeTitle.text = (self.infoDictonary["Title"] as! String)
        destView.infoDictonary = self.infoDictonary
        
        
    }


}

